package com.jadeja.weatherapp.base;

import android.content.Context;

import com.jadeja.weatherapp.di.AppComponent;
import com.jadeja.weatherapp.di.AppUtilModule;
import com.jadeja.weatherapp.di.DaggerAppComponent;
import com.jadeja.weatherapp.di.NetworkModule;

import androidx.multidex.MultiDex;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;


/**
 * <h1>AppController</h1>
 * It controls the project
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
public class AppController extends DaggerApplication {

    private AppComponent appComponent;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .netModule(new NetworkModule())
                .appUtilModule(new AppUtilModule())
                .build();
        appComponent.inject(this);
        return appComponent;
    }


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
