package com.jadeja.weatherapp.di;


import com.jadeja.weatherapp.ui.MainActivity;
import com.jadeja.weatherapp.ui.MainModule;
import com.jadeja.weatherapp.ui.MainUtilModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>ActivityBindModule</h1>
 * Add all activities and contribute related modules to the android injector
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */

@Module
public interface ActivityBindModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainModule.class, MainUtilModule.class})
    MainActivity mainActivity();
}
