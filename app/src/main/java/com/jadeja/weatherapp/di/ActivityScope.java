package com.jadeja.weatherapp.di;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * <h1>ActivityScope</h1>
 * Defines scope
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
