package com.jadeja.weatherapp.di;


import android.app.Application;

import com.jadeja.weatherapp.base.AppController;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * <h1>AppComponent</h1>
 * Contains all application level modules/components
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
@Singleton
@Component(modules = {AppModule.class, AppUtilModule.class, NetworkModule.class, ActivityBindModule.class, AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {
    void inject(AppController appController);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        @BindsInstance
        AppComponent.Builder netModule(NetworkModule networkModule);

        @BindsInstance
        AppComponent.Builder appUtilModule(AppUtilModule appUtilModule);

        AppComponent build();
    }
}
