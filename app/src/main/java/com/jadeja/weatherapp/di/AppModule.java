package com.jadeja.weatherapp.di;


import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>AppModule</h1>
 * Application module
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
@Module
public abstract class AppModule {
    @Binds
    abstract Context bindContext(Application application);
}
