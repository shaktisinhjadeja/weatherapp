package com.jadeja.weatherapp.di;


import android.content.Context;

import com.google.gson.Gson;
import com.jadeja.weatherapp.managers.TypefaceManager;

import javax.inject.Singleton;

import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.Module;
import dagger.Provides;

/**
 * <h1>AppUtilModule</h1>
 * Contains app level utility. provide utility which you want provide app level
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
@Module
public class AppUtilModule {
    @Provides
    @Singleton
    Gson getGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    TypefaceManager typefaceManager(Context context) {
        return new TypefaceManager(context);
    }

    @Provides
    @Singleton
    LinearLayoutManager linearLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }
}
