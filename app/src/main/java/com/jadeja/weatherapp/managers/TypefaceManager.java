package com.jadeja.weatherapp.managers;


import android.content.Context;
import android.graphics.Typeface;

import javax.inject.Inject;

/**
 * <h1>TypefaceManager</h1>
 * It contains font family and provides it.
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class TypefaceManager {

    private Typeface thin;
    private Typeface regular;
    private Typeface bold;

    @Inject
    public TypefaceManager(Context context) {
        this.thin = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_thin.ttf");
        this.regular = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf");
        this.bold = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_black.ttf");
    }

    public Typeface getThin() {
        return thin;
    }

    public Typeface getRegular() {
        return regular;
    }

    public Typeface getBold() {
        return bold;
    }
}
