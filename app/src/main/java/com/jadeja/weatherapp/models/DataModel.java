package com.jadeja.weatherapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h1>DataModel</h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class DataModel implements Serializable {
    @SerializedName("location")
    @Expose
    Location location;

    @SerializedName("forecast")
    @Expose
    Forecast forecast;

    public Location getLocation() {
        return location;
    }

    public Forecast getForecast() {
        return forecast;
    }
}
