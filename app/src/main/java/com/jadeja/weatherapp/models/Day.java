package com.jadeja.weatherapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h1></h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class Day implements Serializable {
    @SerializedName("avgtemp_c")
    @Expose
    private Double temperature;

    public Double getTemperature() {
        return temperature;
    }
}
