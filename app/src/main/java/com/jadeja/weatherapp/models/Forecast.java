package com.jadeja.weatherapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

/**
 * <h1></h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class Forecast implements Serializable {
    @SerializedName("forecastday")
    @Expose
    private List<ForecastDay> forecastDays;

    public List<ForecastDay> getForecastDays() {
        return forecastDays;
    }
}
