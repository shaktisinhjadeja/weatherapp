package com.jadeja.weatherapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Calendar;

/**
 * <h1></h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class ForecastDay implements Serializable {
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("day")
    @Expose
    private Day day;

    public String getDate() {
        return date;
    }

    public Day getDay() {
        return day;
    }
}
