package com.jadeja.weatherapp.networking;


import com.jadeja.weatherapp.models.DataModel;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * <h1>ApiService</h1>
 * All the api defines here
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
public interface ApiService {

    /**
     * It fetches the data from server and provides to app
     *
     * @param apiKey : authenticated APIKEY
     * @param query  : current location (city name/ latlong / zipcode)
     * @param days   : number of days daya
     * @return : json response what are converting to DataModel
     */

    @GET("forecast.json")
    Observable<Response<DataModel>> weatherOfWeek(@Query("key") String apiKey, @Query("q") String query, @Query("days") String days);
}
