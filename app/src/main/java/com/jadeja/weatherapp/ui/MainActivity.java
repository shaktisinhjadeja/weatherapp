package com.jadeja.weatherapp.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jadeja.weatherapp.R;
import com.jadeja.weatherapp.managers.TypefaceManager;
import com.jadeja.weatherapp.models.DataModel;
import com.jadeja.weatherapp.utils.Util;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainContract.View, LocationListener {

    private static final float DISTANCE = 10;
    private static final long TIME_SPAN = 5000;
    private static final int ACCESS_LOCATION_PERMISSION = 135;
    private static final int PERMISSION_FROM_SETTINGS = 579;
    private LocationManager locationManager;

    @Inject
    MainContract.Presenter presenter;
    @Inject
    TypefaceManager typefaceManager;
    @Inject
    TemperatureAdapter adapter;
    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.tvTemperature)
    TextView tvTemperature;
    @BindView(R.id.tvCity)
    TextView tvCity;
    @BindView(R.id.listTemperature)
    RecyclerView listTemperature;

    @BindView(R.id.llError)
    LinearLayout llError;
    @BindView(R.id.tvError)
    TextView tvError;
    @BindView(R.id.btnRetry)
    Button btnRetry;
    @BindView(R.id.ivLoader)
    ImageView ivLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocationAccessPermission();
        applyFonts();

        listTemperature.addItemDecoration(new DividerItemDecoration(listTemperature.getContext(), DividerItemDecoration.VERTICAL));
        listTemperature.setLayoutManager(layoutManager);
        listTemperature.setAdapter(adapter);
    }

    @OnClick(R.id.btnRetry)
    public void retry() {
        checkLocationAccessPermission();
        showProgress();
    }

    /**
     * Applies font family to the view
     */
    private void applyFonts() {
        tvTemperature.setTypeface(typefaceManager.getBold());
        tvCity.setTypeface(typefaceManager.getThin());
        tvError.setTypeface(typefaceManager.getThin());
        btnRetry.setTypeface(typefaceManager.getRegular());
    }

    /**
     * Checks the location permission for above android M os, if its already there then fetches the current location
     * otherwise asks for the permission.
     */
    private void checkLocationAccessPermission() {
        showProgress();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_LOCATION_PERMISSION);
        } else {
            provideLocationManager();
        }
    }

    /**
     * Requests for the update location in TIMESPAN and DISTANCE
     */
    @SuppressLint("MissingPermission")
    private void provideLocationManager() {
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_SPAN, DISTANCE, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ACCESS_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                provideLocationManager();
            } else {
                permissionDeniedAlert();
            }
        }
    }

    /**
     * Displaies the alert box eih OK and GOTO SETTINGS button
     * OK : it will re ask permission
     * GOTO SETTINGS : It will redirect app setting.
     */
    private void permissionDeniedAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.permission_denied);
        builder.setMessage(R.string.location_permission_denied_message);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkLocationAccessPermission();
            }
        });
        builder.setNeutralButton(R.string.go_to_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, PERMISSION_FROM_SETTINGS);
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PERMISSION_FROM_SETTINGS:
                    checkLocationAccessPermission();
                    break;
            }
        }
    }

    @Override
    public void showProgress() {
        ivLoader.startAnimation(Util.rotateAnimation());
        ivLoader.setVisibility(View.VISIBLE);
        llError.setVisibility(View.GONE);
        listTemperature.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        ivLoader.setVisibility(View.GONE);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showData(DataModel data) {
        tvTemperature.setText(Math.round(data.getForecast().getForecastDays().get(0).getDay().getTemperature()) + getString(R.string.degree));
        tvCity.setText(data.getLocation().getName());
        listTemperature.setVisibility(View.VISIBLE);
        llError.setVisibility(View.GONE);
    }

    @Override
    public void showError(int error_message) {
        listTemperature.setVisibility(View.GONE);
        llError.setVisibility(View.VISIBLE);
        tvError.setText(getString(error_message));
    }

    @Override
    public void onLocationChanged(Location location) {
        presenter.weatherHistory("" + location.getLatitude() + "," + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

}
