package com.jadeja.weatherapp.ui;


import com.jadeja.weatherapp.models.DataModel;

/**
 * <h1>MainContract</h1>
 *
 * @author Shaktisisnh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public interface MainContract {

    interface View {
        /**
         * It displays progress bar
         */
        void showProgress();

        /**
         * It hides progress bar
         */
        void hideProgress();

        /**
         * It shows the data of current location
         *
         * @param body : Data model
         */
        void showData(DataModel body);

        /**
         * shows the error screen with error message
         *
         * @param error_message : error message
         */
        void showError(int error_message);
    }

    interface Presenter {
        /**
         * Fetch's this weeks weather
         *
         * @param query : city name / lat long / zipcode
         */
        void weatherHistory(String query);

    }
}
