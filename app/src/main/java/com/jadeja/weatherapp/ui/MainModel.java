package com.jadeja.weatherapp.ui;


import com.jadeja.weatherapp.models.ForecastDay;

import java.util.List;

import javax.inject.Inject;

/**
 * <h1>MainModel</h1>
 * Gets, sets the data and do other operation to data
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */

public class MainModel {

    @Inject
    TemperatureAdapter adapter;
    @Inject
    List<ForecastDay> forecastDays;

    @Inject
    MainModel() {
    }

    public void setData(List<ForecastDay> forecastDays) {
        this.forecastDays.clear();
        forecastDays.remove(0);
        this.forecastDays.addAll(forecastDays);
        adapter.notifyDataSetChanged();
    }
}
