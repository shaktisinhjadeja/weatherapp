package com.jadeja.weatherapp.ui;


import android.app.Activity;

import com.jadeja.weatherapp.di.ActivityScope;

import dagger.Binds;
import dagger.Module;

/**
 * <h1>MainModule</h1>
 * View realated module
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */

@Module
public interface MainModule {

    @ActivityScope
    @Binds
    MainContract.Presenter presenter(MainPresenter presenter);

    @ActivityScope
    @Binds
    MainContract.View view(MainActivity activity);


    @ActivityScope
    @Binds
    Activity activity(MainActivity activity);
}
