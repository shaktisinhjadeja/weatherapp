package com.jadeja.weatherapp.ui;


import com.jadeja.weatherapp.R;
import com.jadeja.weatherapp.models.DataModel;
import com.jadeja.weatherapp.networking.ApiService;
import com.jadeja.weatherapp.utils.Constants;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h1>MainPresenter</h1>
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class MainPresenter implements MainContract.Presenter {

    @Inject
    ApiService apiService;
    @Inject
    MainContract.View view;
    @Inject
    MainModel model;

    @Inject
    MainPresenter() {
    }

    @Override
    public void weatherHistory(String city) {
        apiService.weatherOfWeek(Constants.API_KEY, city, "7")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Response<DataModel>>() {
                    @Override
                    public void onNext(Response<DataModel> response) {
                        view.hideProgress();
                        if (response.code() == 200) {
                            view.showData(response.body());
                            model.setData(response.body().getForecast().getForecastDays());
                        } else {
                            view.showError(R.string.error_message);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideProgress();
                        view.showError(R.string.error_message);
                    }

                    @Override
                    public void onComplete() {
                        view.hideProgress();
                    }
                });
    }

}
