package com.jadeja.weatherapp.ui;


import com.jadeja.weatherapp.di.ActivityScope;
import com.jadeja.weatherapp.managers.TypefaceManager;
import com.jadeja.weatherapp.models.ForecastDay;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

/**
 * <h1>MainUtilModule</h1>
 * Utilities that only want to use on View (Activity/Fragment)
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
@Module
public class MainUtilModule {

    @ActivityScope
    @Provides
    List<ForecastDay> forecasts() {
        return new ArrayList();
    }

    @ActivityScope
    @Provides
    TemperatureAdapter temperatureAdapter(List<ForecastDay> forecasts, TypefaceManager typefaceManager) {
        return new TemperatureAdapter(forecasts, typefaceManager);
    }
}
