package com.jadeja.weatherapp.ui;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jadeja.weatherapp.R;
import com.jadeja.weatherapp.managers.TypefaceManager;
import com.jadeja.weatherapp.models.ForecastDay;
import com.jadeja.weatherapp.utils.Util;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


/**
 * <h1>TemperatureAdapter</h1>
 * Displaies the list of temperatures
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class TemperatureAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<ForecastDay> forecastDays;
    private TypefaceManager typefaceManager;

    @Inject
    TemperatureAdapter(List<ForecastDay> forecastDays, TypefaceManager typefaceManager) {
        this.forecastDays = forecastDays;
        this.typefaceManager = typefaceManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false), typefaceManager);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ForecastDay forecastDay = forecastDays.get(position);
        holder.tvTemperature.setText(Math.round(forecastDay.getDay().getTemperature()) + " C");
        holder.tvDay.setText(Util.getDay(forecastDay.getDate()));
    }

    @Override
    public int getItemCount() {
        return forecastDays.size();
    }
}
