package com.jadeja.weatherapp.ui;


import android.view.View;
import android.widget.TextView;

import com.jadeja.weatherapp.R;
import com.jadeja.weatherapp.managers.TypefaceManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h1>ViewHolder</h1>
 * Holds the TemperatureAdapter's views
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDay)
    TextView tvDay;
    @BindView(R.id.tvTemperature)
    TextView tvTemperature;

    public ViewHolder(@NonNull View itemView, TypefaceManager typefaceManager) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        if (typefaceManager != null) {
            tvDay.setTypeface(typefaceManager.getRegular());
            tvTemperature.setTypeface(typefaceManager.getRegular());
        }
    }
}
