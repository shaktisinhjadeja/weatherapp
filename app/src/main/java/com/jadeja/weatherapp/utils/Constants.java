package com.jadeja.weatherapp.utils;


/**
 * <h1>Constants</h1>
 * Contains all the constant values
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 26 July 2019
 */
public interface Constants {
    String API_VERSION = "v1";
    String BASE_URL = "http://api.apixu.com/" + API_VERSION + "/";
    String API_KEY = "924b8d681c6342cda5031446192707";
}
