package com.jadeja.weatherapp.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.text.format.DateFormat;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <h1>Util</h1>
 * Functionality which we want to use app level
 *
 * @author Shaktisinh Jadeja
 * @version 1.0
 * @since 27 July 2019
 */
public class Util {

    public static String getDay(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = format.parse(dateString);
            return (String) DateFormat.format("EEEE", date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    public static Animation rotateAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(9000);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatMode(Animation.INFINITE);
        return rotate;
    }
}
